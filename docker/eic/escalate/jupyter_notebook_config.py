# Configuration file for jupyter-notebook.

#------------------------------------------------------------------------------
# NotebookApp(JupyterApp) configuration
#------------------------------------------------------------------------------

print("-----------NOTEBOOK APP CONFIG----------------")
## Dict of Python modules to load as notebook server extensions.Entry values can
#  be used to enable and disable the loading ofthe extensions. The extensions
#  will be loaded in alphabetical order.
c.NotebookApp.nbserver_extensions = {
    'pyjano.notebook_extension': True
}
