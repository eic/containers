## Set the Access-Control-Allow-Origin header
#  
#  Use '*' to allow any origin to access your server.
#  
#  Takes precedence over allow_origin_pat.
c.ServerApp.allow_origin = '*'

c.Spawner.args = ["--ServerApp.allow_origin=*"]
c.JupyterHub.tornado_settings = {
    'headers': {
        'Access-Control-Allow-Origin': '*',
    },
}

print("\n\n!!!!!!!!!!! SERVER_APP CONFIG !!!!!!!!!!!!!!!!!!!\n\n")
## Extra paths to search for serving static files.
#  
#  This allows adding javascript/css to be available from the notebook server
#  machine, or overriding individual files in the IPython
#c.NotebookApp.extra_static_paths = ['/container/app/jupyter_extensions/static']

# ROOT javascript is there:
import os
if 'ROOTSYS' in os.environ:
    root_js_path = os.path.join(os.environ['ROOTSYS'], 'js')
    print("root_js_path = '{}'".format(root_js_path))
    c.ServerApp.extra_static_paths.append(root_js_path)
else:
    print("NO ROOT JS FOUND")

print("c.ServerApp.extra_static_paths: ", c.ServerApp.extra_static_paths)