
# ejana-gui container

## Introduction

This container is built upon the ejana-app image giving access to
all of the software it contains plus a graphical desktop environment.
The original Docker image source is copied into this container in
the /container/ejana-gui directory. The latest version can be found
on the web here:

https://gitlab.com/eic/containers/tree/master/docker/eic/ejana-gui

With this container you can run GUI programs in 3 ways:

1. Via a full desktop environment in your web browser
2. Via a VNC client on the host
3. Via an X11 server on the host

All three require some software on the host to display the graphics.
See details below on each.

## Quick Start

1. docker run -it --rm -p 6080:6080 -p 5901:5901 electrononcollider/ejana-gui

2. point your web browser to: <http://localhost:6080>

  OR

  point your VNC client to: localhost:5901



## Full Desktop Environment via Web Browser

This is the recommended way to run the desktop environment as web
browsers tend to be more compatible and be better optimized for the
host's graphics system. To use it, just start the container and aim your
web browser to the appropriate port on the localhost.

```
docker run -it --rm -p 6080:6080 -p 5901:5901 electrononcollider/ejana-gui
```
then click <http://localhost:6080>

If you get an error message saying the port is already in use, check that 
you don't already have a container running. If you want to run more than
one container or some other program is using the default 6080 port on
your host, then you can change the port on the host side. e.g.

```
docker run -it --rm -p 6081:6080 -p 5901:5901 electrononcollider/ejana-gui
```
then click <http://localhost:6081>

## Full Desktop Environment via VNC client

The web browser solution above actually uses a VNC server running
inside the container and with port 5901 mapped, you can access this
directly with a VNC client on your host. For example:

```
docker run -it --rm -p 6080:6080 -p 5901:5901 electrononcollider/ejana-gui
(linux) vncviewer localhost:1
(macos) open vnc://localhost:5901
```
Similar to the web browser instructions above, if you find a conflict for
port 5901 on the host, you can change it to another port that maps to
5901 in the container.

```
docker run -it --rm -p 6080:6080 -p 5902:5901 electrononcollider/ejana-gui
(linux) vncviewer localhost:2
(macosx) open vnc://localhost:5902
```
## Using X11

You may use your host's existing X11 server to open windows from the
container. This may integrate better into your host's graphical environment
if that is your preference. To do this you must grant access to the container
(which acts as a seperate computer with its own IP address). To maintain
tight security on your host, you should grant access only to the container.
In practice, this has not always worked and access from any client was
needed. Only do this if you are on a secure network.

```
xhost +
(linux) export DISPLAY=`hostname`:0
(macosx) export DISPLAY=host.docker.internal:0
docker run -it --rm -e DISPLAY=$DISPLAY electrononcollider/ejana-gui bash
 $ xterm
```
## Using dsh to set uid, gid, and mount host home directory

A script is packaged with the ejana-gui container to help create a user in
the container with the same username, uid, and gid as the host. It will also
map your local home directory to a directory inside the container. This will
make it much easier to work with files outside of the container's file system.
This is important if you want to save anything after the container is closed.
As a bonus, it will map the ports and set up the environment so that all 3 of
the graphics methods describe can work.

The script is called "dsh" and must be copied out of the container to the
host file system to use it. This only needs to be done once:

```
docker run --rm electrononcollider/ejana-gui cat /eic/app/dsh/dsh > dsh
chmod +x dsh
```
Run it like this:

```
dsh electrononcollider/ejana-gui
```
If no container is currently running, one will be created. Subsequent
invocations of the above command will open additional shells in the same
container (using docker exec). If you wish to stop the container do this:

```
dsh -s electrononcollider/ejana-gui
```

If you want to use the full desktop environment with your web browser or
VNC client, do the following:

```
cp -r /container/ejana-gui/dot_config .config
cp -r /container/ejana-gui/dot_Xclients .Xclients
xstart
```

Run "dsh -h" for a full usage statement, including some helpful environment
variables.


Please direct any questions to:
David Lawrence <davidl@jlab.org>
