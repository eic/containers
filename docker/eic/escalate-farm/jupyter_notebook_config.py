# Configuration file for jupyter-notebook.

#------------------------------------------------------------------------------
# NotebookApp(JupyterApp) configuration
#------------------------------------------------------------------------------

## Set the Access-Control-Allow-Origin header
#  
#  Use '*' to allow any origin to access your server.
#  
#  Takes precedence over allow_origin_pat.
c.NotebookApp.allow_origin = '*'

c.Spawner.args = ["--NotebookApp.allow_origin=*"]
c.JupyterHub.tornado_settings = {
    'headers': {
        'Access-Control-Allow-Origin': '*',
    },
}

print("\n\n!!!!!!!!!!! CONFIG !!!!!!!!!!!!!!!!!!!\n\n")
## Extra paths to search for serving static files.
#  
#  This allows adding javascript/css to be available from the notebook server
#  machine, or overriding individual files in the IPython
c.NotebookApp.extra_static_paths = ['/container/app/jupyter_extensions/static']

# ROOT javascript is there:
import os
if 'ROOTSYS' in os.environ:
    root_js_path = os.path.join(os.environ['ROOTSYS'], 'js')
    print("root_js_path = '{}'".format(root_js_path))
    c.NotebookApp.extra_static_paths.append(root_js_path)
else:
    print("NO ROOT JS FOUND")

print("c.NotebookApp.extra_static_paths: ", c.NotebookApp.extra_static_paths)
## Dict of Python modules to load as notebook server extensions.Entry values can
#  be used to enable and disable the loading ofthe extensions. The extensions
#  will be loaded in alphabetical order.
c.NotebookApp.nbserver_extensions = {
    'pyjano.notebook_extension': True
}
