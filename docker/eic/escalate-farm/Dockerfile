#
# docker build -t electronioncollider/escalate:latest .
# docker build -t electronioncollider/escalate:latest --build-arg BUILD_THREADS=24 .
# docker build --no-cache -t electronioncollider/escalate:latest --build-arg BUILD_THREADS=24 .
# docker push electronioncollider/escalate:latest
#
# docker tag electronioncollider/escalate:latest electronioncollider/escalate:1.0.0
# docker push electronioncollider/escalate:1.0.0
# 
# docker run --rm -it -p8888:8888 electronioncollider/escalate:latest

FROM electronioncollider/eic-mceg:latest

USER eicuser
# Number of build threads
ARG BUILD_THREADS=8


# JUPYTER and stuff!
ADD jupyter_notebook_config.py /etc/jupyter/
ADD lab.jupyterlab-workspace /home/eicuser/.jupyter/lab/workspaces/
ADD .bashrc /home/eicuser/
ADD plugin.jupyterlab-settings /home/eicuser/.jupyter/lab/user-settings/@jupyterlab/docmanager-extension/plugin.jupyterlab-settings


USER root
WORKDIR ${APP_ROOT}
# clone epic data
RUN sudo chown -R eicuser:eicuser /home/eicuser/.jupyter &&\
    sudo usermod --shell /bin/bash eicuser &&\
    git config --global http.sslVerify false &&\
    git clone https://gitlab.com/eic/escalate/jupyter_extensions.git &&\
    python3 -m jupyter labextension link ${APP_ROOT}/jupyter_extensions/root_renderer &&\
    python3 -m jupyter labextension install @jupyter-widgets/jupyterlab-manager@2.0 &&\
    python3 -m jupyter labextension install jupyterlab-plotly &&\
    python3 -m jupyter lab workspaces import /home/eicuser/.jupyter/lab/workspaces/lab.jupyterlab-workspace


# SSH service
# SSH can be run with adding runssh command
RUN mkdir /var/run/sshd &&\
    echo 'root:electron-ion-collider' | chpasswd &&\
    echo 'eicuser:eicuser' | chpasswd &&\
    sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config &&\
    echo 'X11UseLocalhost no' >> /etc/ssh/sshd_config &&\
    /usr/bin/ssh-keygen -A

#RUN echo 'HostKey /etc/ssh/ssh_host_rsa_key' >> /etc/ssh/sshd_config
# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile
ADD runssh /home/eicuser/.local/bin/runssh
EXPOSE 22


# eicuser

USER eicuser
# last configurations
WORKDIR /home/eicuser/
RUN sudo chmod +x /home/eicuser/.local/bin/runssh &&\
    sudo chown -R eicuser:eicuser /home/eicuser/  && \
    sudo chown eicuser:eicuser /home/eicuser/  &&\
    sed -i 's/\r$//' /home/eicuser/.bashrc &&\
    cd /home/eicuser/workspace/data &&\
    unzip -o herwig6_20k.zip

#ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/container/app/eic-smear/eic-smear-master/lib

# Starting jupyter command
WORKDIR /home/eicuser/workspace

ENTRYPOINT ["/tini", "--", "bash", "-c", "source /container/app/userenv.sh && \"$@\"", "-s"]

# Update ejana so it can be just one of the last layers:
USER root

# for futher versions without root reinstallations
# RUN export EJPM_DATA_PATH=/container/app/ejpm_data &&\
#     python3 -m pip install --no-cache-dir --upgrade "ejpm==0.3.20" &&\
#     ejpm config ejana branch=master &&\
#     ejpm config g4e branch=master &&\
#     ejpm rm ejana && ejpm install ejana &&\
#     ejpm rm g4e && ejpm install g4e

USER eicuser
RUN git fetch && git reset --hard origin/master && git pull && echo "hello world!!"
    

CMD jupyter lab --ip=0.0.0.0 --no-browser \
    --NotebookApp.custom_display_url=http://127.0.0.1:8888 \
    --NotebookApp.token='' && runssh

# =============================
#    E N V I R O N M E N T
# =============================
#
#  Basically the next environment is set automatically by ejpm and in docker
#  by calling 


# acts
# =============================
ENV ACTS_DIR="/container/app/acts/acts-${VERSION_ACTS}" \
    LD_LIBRARY_PATH="/container/app/acts/acts-${VERSION_ACTS}/lib:$LD_LIBRARY_PATH" \
    CMAKE_PREFIX_PATH="/container/app/acts/acts-${VERSION_ACTS}/share/cmake/Acts:$CMAKE_PREFIX_PATH"

# =============================
# clhep
# =============================
ENV CLHEP="/container/app/clhep/clhep-${VERSION_CLHEP}" \
    CLHEP_BASE_DIR="/container/app/clhep/clhep-${VERSION_CLHEP}" \
    CLHEP_INCLUDE_DIR="/container/app/clhep/clhep-${VERSION_CLHEP}/include" \
    CLHEP_LIB_DIR="/container/app/clhep/clhep-${VERSION_CLHEP}/lib" \
    PATH="/container/app/clhep/clhep-${VERSION_CLHEP}/bin:$PATH" \
    LD_LIBRARY_PATH="/container/app/clhep/clhep-${VERSION_CLHEP}/lib:$LD_LIBRARY_PATH"

# =============================
# eic-smear
# =============================
ENV EIC_SMEAR_HOME="/container/app/eic-smear/eic-smear-${VERSION_EIC_SMEAR}" \
    LD_LIBRARY_PATH="/container/app/eic-smear/eic-smear-${VERSION_EIC_SMEAR}/lib:$LD_LIBRARY_PATH" \
    ROOT_INCLUDE_PATH="/container/app/eic-smear/eic-smear-${VERSION_EIC_SMEAR}/include:$ROOT_INCLUDE_PATH"

# =============================
# eigen3
# =============================
ENV CMAKE_PREFIX_PATH="/container/app/eigen3/eigen3-${VERSION_EIGEN3}/share/eigen3/cmake/:$CMAKE_PREFIX_PATH"

# =============================
# ejana
# =============================
# ejana uses old (but not yet changed) 'dev' build, so it is located not in version, but 'dev' folder
ENV EJANA_HOME="/container/app/ejana/ejana-dev/compiled" \
    JANA_PLUGIN_PATH="/container/app/ejana/ejana-dev/compiled/plugins:$JANA_PLUGIN_PATH" \
    PATH="/container/app/ejana/ejana-dev/compiled/bin:$PATH" \
    LD_LIBRARY_PATH="/container/app/ejana/ejana-dev/compiled/lib:$LD_LIBRARY_PATH"

# =============================
# g4e
# =============================
# G4E also uses old (but not yet changed) 'dev' build, so it is located not in version, but 'dev' folder
ENV PATH="/container/app/g4e/g4e-dev/bin:$PATH" \
    PYTHONPATH="/container/app/g4e/g4e-dev/python:$PYTHONPATH" \
    G4E_HOME="/container/app/g4e/g4e-dev" \
    G4E_MACRO_PATH="/container/app/g4e/g4e-dev"

# =============================
# geant
# =============================
ENV PATH "/container/app/geant/geant-${VERSION_GEANT4}/bin:$PATH"

# =============================
# genfit
# =============================
ENV GENFIT="/container/app/genfit/genfit-${VERSION_GENFIT}" \
    GENFIT_DIR="/container/app/genfit/genfit-${VERSION_GENFIT}" \
    LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/container/app/genfit/genfit-${VERSION_GENFIT}/lib"

# =============================
# hepmc
# =============================
ENV PATH="/container/app/hepmc/hepmc-${VERSION_HEPMC}/bin:$PATH" \
    HEPMC_DIR="/container/app/hepmc/hepmc-${VERSION_HEPMC}" \
    LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/container/app/hepmc/hepmc-${VERSION_HEPMC}/lib:/container/app/hepmc3/hepmc3-${VERSION_HEPMC3}/lib"

# =============================
# jana
# =============================
ENV JANA_HOME="/container/app/jana/jana-${VERSION_JANA}" \
    JANA_PLUGIN_PATH="$JANA_PLUGIN_PATH:$JANA_HOME/plugins" \
    PATH="$JANA_HOME/bin:$PATH"

# =============================
# root
# =============================
ENV MANPATH="/container/app/root/root-${VERSION_CERN_ROOT}/man" \
    SHLIB_PATH="/container/app/root/root-${VERSION_CERN_ROOT}/lib" \
    DYLD_LIBRARY_PATH="/container/app/root/root-${VERSION_CERN_ROOT}/lib" \
    CMAKE_PREFIX_PATH="/container/app/root/root-${VERSION_CERN_ROOT}:$CMAKE_PREFIX_PATH" \
    PYTHONPATH="/container/app/root/root-${VERSION_CERN_ROOT}/lib:$PYTHONPATH" \
    LIBPATH="/container/app/root/root-${VERSION_CERN_ROOT}/lib" \
    JUPYTER_PATH="/container/app/root/root-${VERSION_CERN_ROOT}/etc/notebook" \
    ROOTSYS="/container/app/root/root-${VERSION_CERN_ROOT}" \
    LD_LIBRARY_PATH="/container/app/root/root-${VERSION_CERN_ROOT}/lib:$LD_LIBRARY_PATH" \
    PATH="/container/app/root/root-${VERSION_CERN_ROOT}/bin:$PATH"

# vgm
ENV VGM_DIR="/container/app/vgm/vgm-${VERSION_VGM}" \
    LD_LIBRARY_PATH="/container/app/vgm/vgm-${VERSION_VGM}/lib:$LD_LIBRARY_PATH"

# Resource file paths
# - Datasets
ENV   G4NEUTRONHPDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/G4NDL4.6" \
             G4LEDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/G4EMLOW7.9.1" \
     G4LEVELGAMMADATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/PhotonEvaporation5.5" \
    G4RADIOACTIVEDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/RadioactiveDecay5.4" \
     G4PARTICLEXSDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/G4PARTICLEXS2.1" \
            G4PIIDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/G4PII1.3" \
    G4REALSURFACEDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/RealSurface2.1.1" \
         G4SAIDXSDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/G4SAIDDATA2.0" \
           G4ABLADATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/G4ABLA3.1" \
           G4INCLDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/G4INCL1.0" \
     G4ENSDFSTATEDATA="/container/app/geant/geant-${VERSION_GEANT4}/share/Geant4-${VERSION_GEANT4}/data/G4ENSDFSTATE2.2"

#-----------------------------------------------------------------------
USER root
# Copy docker files
RUN mkdir -p ${CONTAINER_ROOT}/escalate &&\
    date > ${CONTAINER_ROOT}/escalate/creation-time.txt &&\
    export EJPM_DATA_PATH=/container/app/ejpm_data &&\
    ejpm rm ejana && ejpm install ejana
ADD Dockerfile ${CONTAINER_ROOT}/escalate/
ADD jupyter_notebook_config.py ${CONTAINER_ROOT}/escalate/jupyter_notebook_config.py
ADD .bashrc ${CONTAINER_ROOT}/escalate/
ADD runssh ${CONTAINER_ROOT}/escalate/
ADD lab.jupyterlab-workspace ${CONTAINER_ROOT}/escalate/

USER eicuser
