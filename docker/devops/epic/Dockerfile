# Build:
#   docker build -t epic -t eicdev/epic:latest .
# Sometimes one need to pull newer versions:
#   docker pull 
# No cache build:
#   docker build --no-cache -t epic -t eicdev/epic:latest .
# Push: 
#   docker push eicdev/epic:latest
# Run versions:
#     docker run -p 8888:8888 eicdev/epic:latest
#     docker run --rm -it -p 8888:8888 eicdev/epic:latest jupyter lab --ip=0.0.0.0
# Run bash:
#     docker run --rm -it -p 8888:8888 eicdev/epic:latest bash
# Link existing directory
#    docker run -v /home/romanov/eic/ddata/epw:/home/eicuser/epw --rm -it -p 8888:8888 eicdev/epic:latest bash
# Example 
#    docker run -v /home/romanov/eic/epw:/home/eicuser/epw -v /home/romanov/eic/pyjano:/container/app/pyjano --rm -it -p 8888:8888 eicdev/epic:latest bash
# 

FROM electronioncollider/ejana-app:latest

USER root
WORKDIR ${APP_ROOT}

COPY  pip.conf /etc/pip.conf

# Python libs, jupyter, ejpm
RUN python3 -m pip --no-cache-dir  install --upgrade jupyterlab uproot pandas matplotlib seaborn ipywidgets metakernel&&\
    python3 -m pip --no-cache-dir  install --upgrade --force-reinstall ejpm

# Install the git-lfs repo, 
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | bash - &&\
    yum --setopt=tsflags=nodocs install -y git-lfs

# Hello JLab my old friend
# I came to talk to you again... 
RUN git config --global http.sslVerify false

# Add Tini entry point
# What is the advantage of tini?
# https://github.com/krallin/tini/issues/8
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

# micro CLI text editor 
WORKDIR ${APP_ROOT}
RUN wget --quiet https://github.com/zyedidia/micro/releases/download/v1.4.1/micro-1.4.1-linux64.tar.gz &&\
    tar xf micro-1.4.1-linux64.tar.gz &&\
    chmod +x micro-1.4.1/micro &&\    
    cp micro-1.4.1/micro /usr/bin &&\
    rm -rf micro-1.4.1 micro-1.4.1-linux64.tar.gz 

USER eicuser

# update ejana
RUN ejpm rm ejana && ejpm install ejana --single

# clone epic data
RUN git config --global http.sslVerify false &&\
    git clone https://gitlab.com/eic/epic.git &&\
    git clone https://gitlab.com/eic/pyjano.git &&\
    cd pyjano &&\
    sudo python3 -m pip --no-cache-dir  install -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:${APP_ROOT}/pyjano"


WORKDIR /home/eicuser/
RUN git clone https://gitlab.com/eic/epw.git &&\
    echo "Updated EPW repository"

# jupyterlab-git
# WORKDIR ${APP_ROOT}
# RUN git clone https://github.com/jupyterlab/jupyterlab-git.git &&\
#     cd jupyterlab-git &&\
#     npm install &&\ 
#     npm run build &&\
#     sudo python3 -m jupyter labextension link .&&\
#     sudo python3 -m pip install . &&\
#     sudo python3 -m jupyter serverextension enable --py jupyterlab_git

# configure jupyterlab
RUN sudo jupyter labextension link ${APP_ROOT}/epic/jupyter/root_renderer &&\
    sudo jupyter labextension install jupyterlab_requirejs &&\
    sudo jupyter labextension install @jupyter-widgets/jupyterlab-manager &&\
    sudo jupyter serverextension enable pyjano.notebook_extension

# sudo jupyter labextension link ${APP_ROOT}/epic/jupyter/requirejsroot &&\

ADD jupyter_notebook_config.py /etc/jupyter/
ADD lab.jupyterlab-workspace /home/eicuser/.jupyter/lab/workspaces/
ADD .bashrc /home/eicuser/

# Set the workspace by default
RUN sudo jupyter lab workspaces import /home/eicuser/.jupyter/lab/workspaces/lab.jupyterlab-workspace

# Fix rootjs jupyterlab problem... the hard way
# https://github.com/root-project/jsroot/issues/166
ADD patch_index_html.py ${APP_ROOT}/pyjano
RUN sudo python3 ${APP_ROOT}/pyjano/patch_index_html.py

# Last configs...
RUN sudo cp /home/eicuser/.local/share/ejpm/env.sh /etc/profile.d/ejpm.sh &&\
    sudo chown -R eicuser:eicuser /home/eicuser/  && \
    sudo chown eicuser:eicuser /home/eicuser/  &&\
    sed -i 's/\r$//' /home/eicuser/.bashrc  

ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/container/app/eic-smear/eic-smear-master/lib

# Unzip some data
WORKDIR /home/eicuser/epw/data
RUN unzip -o herwig6_20k.zip

# Starting jupyter command
WORKDIR /home/eicuser/epw

RUN git pull && echo "Pulled from EPW repo"

CMD source /home/eicuser/.local/share/ejpm/env.sh &&\
    jupyter lab --ip=0.0.0.0 --no-browser \
    --NotebookApp.custom_display_url=http://127.0.0.1:8888 \
    --NotebookApp.token=''

#display_url