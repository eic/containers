# -----------------------------------------------------------------------------
# eicdev/eicrecon-centos7-prereq
#
# This Dockerfile describes a shared base container image for CentOS, with:
#   - basic development tools installed: gcc, python3, cmake, etc.
#   - common configuration: eicuser, eic group, folder structure and rights
#
# No scientific packages (like ROOT or Geant) is present in this image
#
# look README.md for the description and design philosophy of this Dockerfile
#
# -----------------------------------------------------------------------------
#   
# This image is intend to be flattened
# To flatten the image:
# docker export <CONTAINER ID> | docker import - some-image-name:latest
# or better new experimental feature --squash: 
# https://docs.docker.com/engine/reference/commandline/build/#squash-an-images-layers-squash-experimental-only
#
# TO ENABLE EXPERIMENTAL FEATURES!
#      > sudo vim /etc/docker/daemon.json
#   add there: 
#      "experimental": true 
#   restart service and check: 
#      > sudo service docker restart
#      > docker version
#
# TO BUILD WITH SQUASH 
#      > docker build --squash -t eicrecon-centos7-prereq .
#
# TO BUILD WITH PYTHON3 --enable-optimizations compilation flag use:
#      ... --build-arg PYTHON_FLAGS='--enable-optimizations' ...
# 
# -----------------------------------------------------------------------------
# BUILD&PUSH CHEAT SHEET:
#   Basic build & later tag:
#      docker build -t eicrecon-centos7-prereq .
#      docker tag eicrecon-centos7-prereq eicdev/eicrecon-centos7-prereq:latest
#
#   Squashed(flattened) build
#      docker build --squash -t eicdev/eicrecon-centos7-prereq:latest .
#
#   Or build and tag in the same command: 
#      docker build -t eicdev/eicrecon-centos7-prereq:latest .
# 
#   Python build optimization: 
#      docker build --build-arg PYTHON_FLAGS='--enable-optimizations' -t eicdev/eicrecon-centos7-prereq:latest .
#      docker build --squash --build-arg PYTHON_FLAGS='--enable-optimizations' -t eicdev/eicrecon-centos7-prereq:latest .
#
#
#   Push and run
#      docker push eicdev/eicrecon-centos7-prereq:latest
#      docker run --rm -it eicdev/eicrecon-centos7-prereq:latest bash 
#
#   Never run this (!!!) (if you are not in a 'tested well' state)
#      docker build --squash --no-cache -t eicdev/eicrecon-centos7-prereq:latest -t eicrecon-centos7-prereq . && docker push eicdev/eicrecon-centos7-prereq
# 
# -----------------------------------------------------------------------------

FROM centos:7.9.2009

ARG CMAKE_VERSION=3.17.0
ARG EICUSER_ID=1000

# Number of build threads
ARG BUILD_THREADS=8

# Python build flags. ARG to add --enable-optimizations flag to python3 build
# which speeds up some python functions up to 20% but is very long to build
# ARG PYTHON_FLAGS='--enable-optimizations'
ARG PYTHON_FLAGS=''

ENV EIC_ROOT /eic
ENV CONTAINER_ROOT /container
ENV APP_ROOT /container/app
ENV MCEG_ROOT /container/app/mceg

# Build directory structure
RUN mkdir -p ${APP_ROOT} && \
    mkdir -p ${EIC_ROOT}/app

# 'yum clean all' for every RUN with 'yum'
# https://github.com/cms-sw/cms-docker/issues/28
RUN yum install -y http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm &&\
    yum update -y   &&\ 
    yum --setopt=tsflags=nodocs install -y \
        gperf libuuid-devel libxml2-devel wget which sudo bzip2-devel make mlocate scons git \
        expat-devel libffi-devel openssl-devel libsqlite3x-devel && \
    rm -rf /var/cache/yum

# CMake
RUN mkdir -p /tmp/cmake && \
    pushd /tmp/cmake && \
    wget "https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-Linux-x86_64.sh" --no-check-certificate && \
    bash cmake-${CMAKE_VERSION}-Linux-x86_64.sh --prefix=/usr/local --exclude-subdir && \
    popd && \
    rm -rf /tmp/cmake

# GCC
RUN yum install -y centos-release-scl && \
    yum install -y devtoolset-8-gcc* && \
    yum install -y devtoolset-8-gdb* && \
    rm -rf /var/cache/yum

ENV PATH="/opt/rh/devtoolset-8/root/usr/bin:$PATH"
RUN source scl_source enable devtoolset-8

# Python 3
# (!!) WHY WE HAVE TO BUILD PYTHON FROM SOURCES???? 
# Because Cern ROOT pyROOT requires that python and root both are built with the ABI compatible compilers
# BUT! CentOS python is built with 4.8.5 and ROOT will be built with devtoolset-7 GCC 7.3

# This needed for some python frameworks (click, format, globalize)
ENV LC_ALL=en_US.utf-8
ENV LANG=en_US.utf-8

WORKDIR ${APP_ROOT}

RUN git clone --branch=3.7 https://github.com/python/cpython.git python &&\    
    cd python &&\
    ./configure \        
        --enable-loadable-sqlite-extensions \
        --enable-shared \
        --with-lto \
        --with-system-expat \
        --with-system-ffi \
        --enable-ipv6 --with-threads \
        # --with-pydebug 
        --disable-rpath \
        ${PYTHON_FLAGS} \
    && make -j 24 \
    && make -j 24 altinstall \
    && echo '/usr/local/lib/'>> /etc/ld.so.conf \
    && ldconfig \
    && ln -s $(which python3.7) /usr/local/bin/python3 \
    && python3 -m pip install --upgrade pip \
    && rm -rf python
    

# Node js
RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash - &&\
    yum --setopt=tsflags=nodocs install -y nodejs dos2unix vim unzip&&\ 
    rm -rf /var/cache/yum &&\
    ls -latrh

# Create EIC user and give him rights for some directories
RUN useradd -m -u ${EICUSER_ID} -G wheel eicuser   &&\
    echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers

RUN install -d -o eicuser -g eicuser ${EIC_ROOT} && \
    install -d -o eicuser -g eicuser ${CONTAINER_ROOT} && \
    install -d -o eicuser -g eicuser ${APP_ROOT}


# === GIT 2.0+ ===
RUN yum -y remove git* &&\
    yum -y install  https://centos7.iuscommunity.org/ius-release.rpm &&\
    yum -y install  git2u-all &&\
    yum clean all

# Install the git-lfs repo, 
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | bash - &&\
    yum --setopt=tsflags=nodocs install -y git-lfs

# Hello JLab my old friend
# I came to talk to you again... 
RUN git config --global http.sslVerify false

# Add Tini entry point
# What is the advantage of tini?
# https://github.com/krallin/tini/issues/8
ARG TINI_VERSION=v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

# micro CLI text editor 
ARG MICRO_VERSION=1.4.1
WORKDIR ${APP_ROOT}
RUN wget --quiet https://github.com/zyedidia/micro/releases/download/v${MICRO_VERSION}/micro-${MICRO_VERSION}-linux64.tar.gz &&\
    tar xf micro-${MICRO_VERSION}-linux64.tar.gz &&\
    chmod +x micro-${MICRO_VERSION}/micro &&\    
    cp micro-${MICRO_VERSION}/micro /usr/bin &&\
    rm -rf micro-${MICRO_VERSION} micro-${MICRO_VERSION}-linux64.tar.gz 

RUN mkdir -p ${CONTAINER_ROOT}/eicrecon-centos7-prereq
COPY Dockerfile ${CONTAINER_ROOT}/eicrecon-centos7-prereq


# Upgrade pip & install edpm 
#   (ejpm is eic packet manager. It is introduced here as it tracks scientific
#    packets dependencies and 'right' compilation flags. Sinceedpm solution 
#    is well tested we use it here instead of installing packets explicitly 
#    in this dockerfile.)
RUN python3 -m pip --no-cache-dir install --upgrade pip && \
    python3 -m pip --no-cache-dir install appdirs click && \
    python3 -m pip --no-cache-dir install edpm

# install prerequesties
RUN echo $(ejpm req centos7) &&\
    yum -y install $(ejpm req centos7 --all)  && \
    yum clean all

# eicrecon stack
RUN edpm install clhep && edpm clean clhep &&\
    edpm install eigen3 && edpm clean eigen3 &&\    
    edpm install -j${BUILD_THREADS} root && edpm clean root &&\
    edpm install hepmc3 && edpm clean hepmc3 &&\    
    edpm install -j${BUILD_THREADS} geant4 && edpm clean geant4


# aa
RUN python3 -m pip install --user --upgrade --force-reinstall  edpm &&\
    edpm install eicrecon --deps-only     