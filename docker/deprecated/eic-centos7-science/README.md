The image contains:

* [ejpm](https://pypi.org/project/ejpm/) - EIC "package" manager (run ejpm command to see all software)
* [CERN ROOT](https://root.cern.ch/content/release-61804) - 6.18.X
* [Geant4](https://github.com/Geant4/geant4) - 10.6.X
* HENP analysis and reconstruction libraries: genfit, rave, clhep, vgm
* Python analysis tools: jupyterlab, uproot, numpy, pandas, matplotlib, seaborn, etc.

Software is built with 
[RH devtoolset-8](https://access.redhat.com/documentation/en-us/red_hat_developer_toolset/8/html/8.0_release_notes/dts8.0_release) - which provides **GCC** 8.2.1 and other packages.